#DIRECTORIES
Code/ Contains both Server and client side code.

I have used node js with express framework as Server and python as client.

#INSTALLATION
$ npm install express
$ npm install body-parser
$ npm install python-shell


#EVALUATION
The final code will be tested by executing ``runserver.sh`` in the 
server directory, and heading to the browser at the URL 
``http://localhost:8000/``

#TASK
At the http://localhost:8000/ url the user encounters a page that allows 
the user to choose one of two values from a dropdown, enter a value, and 
submit. The two values will be *hash* and *counter*. The result of the submit,
and the request number it came from, will be shown on the right.

Server responses will be in JSON.

##The *hash* action
md5 hash the value sent by the client, and return the string.
The result on the right should show what string the user entered, and its
hash.

##The *counter* action
Second endpoint should increment the user-given value by 1 and return it.
If the user-given value is less than 0, return an error message to the user.
The result on the right show the number the user entered, and the result of
the increment.

##BONUS: The *global-counter* action
A third action can be done for bonus points.
This endpoint will increment the user-given value, with the last valid value 
the user submitted for the *global-counter* action. Valid values are values 
greater than 0. The initial global-counter value can be assumed to be 0.


