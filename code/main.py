# -*- coding: utf-8 -*-
import hashlib
import sys

def md5hasher(data):
    md5hash = hashlib.md5()
    md5hash.update(data)
    hash_value=(md5hash.hexdigest())
    print hash_value
    return;  
       
def counter(data,add):
     if data>0:
        counter_value=data+add
        print str(counter_value)
     else:
        print "Invalid"
     return;

lines = sys.stdin.readlines()
data_name=lines[0]
data_value=lines[1]

while True:
     if data_name=="hash\n":
        md5hasher(data_value)
        break
     elif data_name=="counter\n":
        counter(int(data_value),1)
        break
     elif data_name=="global_counter\n": 
         data_global=lines[2]
	 if data_global=="\n":
	    data_global=0		
         counter(int(data_value),int(data_global))
         break
