var express   = require('express');
var bodyparser=require('body-parser');
var path      = require("path");
var app       =express();
global.myNumber;

app.use(bodyparser());

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.post('/',function(req,res){	
	var user_type = req.body.type
	var user_value = req.body.value
	var jsonString = "{\"key\":\"value\"}";
    console.log("user type:"+user_type);
    console.log("user value:"+user_value);
    
            
	var PythonShell = require('python-shell');
	PythonShell.defaultOptions = { scriptPath:__dirname  };
	var pyshell = new PythonShell('main.py');
	console.log("Calling Python")
	
	if (user_type.toString().trim() === 'counter'){
		global.myNumber= user_value
	}

	if (user_type.toString().trim() === 'global_counter'){
		pyshell.send(user_type).send(user_value).send(global.myNumber).end(function (err) {
		if (err) return done(err);
				done();
		});
		global.myNumber= user_value
	}else{	
	pyshell.send(user_type).send(user_value).end(function (err) {
		if (err) return done(err);
				done();
		});
	}
	
	pyshell.on('message', function (message) {
		 console.log(message);
		 response={
			 Type:user_type,
			 Input:user_value,
			 Result:message
		 }
	res.end(JSON.stringify(response));
	});	
	
	pyshell.end(function (err) {
	  if (err) throw err;
	  console.log('finished');
	});
});

app.listen(8000);

console.log("Running at Port 8000");
